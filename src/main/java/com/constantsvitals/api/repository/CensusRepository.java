package com.constantsvitals.api.repository;

import com.constantsvitals.api.model.entity.Census;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CensusRepository extends JpaRepository<Census, Long> {
    List<Census> findBycenNumFloorOrderByCenNumRoomAsc(int floor);

}
