package com.constantsvitals.api.repository;

import com.constantsvitals.api.model.entity.VitalSignMeasurement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VitalSignMeasurementRepository extends JpaRepository<VitalSignMeasurement, Long> {
    List<VitalSignMeasurement> findByMeasurementCensusCenIdOrderByMeasurementMsrDateHourEndDesc(Long cenId);
}
