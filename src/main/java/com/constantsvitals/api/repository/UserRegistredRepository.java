package com.constantsvitals.api.repository;

import com.constantsvitals.api.model.entity.UserRegistered;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRegistredRepository extends JpaRepository<UserRegistered, Long> {
    UserRegistered findByUsername(String username);
}
