package com.constantsvitals.api.repository;

import com.constantsvitals.api.model.entity.VitalSignType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VitalSignTypeRepository extends JpaRepository<VitalSignType, Long> {

}
