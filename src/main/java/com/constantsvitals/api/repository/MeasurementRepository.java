package com.constantsvitals.api.repository;

import com.constantsvitals.api.model.entity.Census;
import com.constantsvitals.api.model.entity.Measurement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MeasurementRepository extends JpaRepository<Measurement, Long> {
    Measurement findFirstByCensusOrderByMsrDateHourEndDesc(Census census);


}
