package com.constantsvitals.api.controller;

import com.constantsvitals.api.exception.ResourceNotFoundException;
import com.constantsvitals.api.model.entity.VitalSign;
import com.constantsvitals.api.repository.VitalSignRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class VitalSignController {

    @Autowired
    VitalSignRepository vitalSignRepository;

    // Get All
    @Secured({"ROLE_USER"})
    @GetMapping("/vitalsigns")
    public List<VitalSign> getAllVitalSigns() {
        return vitalSignRepository.findAll();
    }

    // Create
    @Secured({"ROLE_USER"})
    @PostMapping("/vitalsigns")
    public VitalSign createVitalSign(@Valid @RequestBody VitalSign vitalSign) {
        return vitalSignRepository.save(vitalSign);
    }

    // Create
    @Secured({"ROLE_USER"})
    @PostMapping("/vitalsignsList")
    public List<VitalSign> createListVitalSign(@Valid @RequestBody List<VitalSign> vitalSigns) {
        return vitalSignRepository.saveAll(vitalSigns);
    }

    // Get a Single
    @Secured({"ROLE_USER"})
    @GetMapping("/vitalsigns/{id}")
    public VitalSign getVitalSignById(@PathVariable(value = "id") Long vitalSignId) {
        return vitalSignRepository.findById(vitalSignId)
                .orElseThrow(() -> new ResourceNotFoundException("VitalSign", "id", vitalSignId));
    }

    // Update
    @Secured({"ROLE_USER"})
    @PutMapping("/vitalsigns/{id}")
    public VitalSign updateVitalSign(@PathVariable(value = "id") Long vitalSignId,
                           @Valid @RequestBody VitalSign vitalSign) {

        VitalSign currentVitalSign = vitalSignRepository.findById(vitalSignId)
                .orElseThrow(() -> new ResourceNotFoundException("VitalSign", "id", vitalSignId));

        currentVitalSign.setVsName(vitalSign.getVsName());
        currentVitalSign.setVitalSignType(vitalSign.getVitalSignType());
        currentVitalSign.setVsNemotechnical(vitalSign.getVsNemotechnical());
        currentVitalSign.setVsLimitMaxValue(vitalSign.getVsLimitMaxValue());
        currentVitalSign.setVsLimitMinValue(vitalSign.getVsLimitMinValue());

        VitalSign updatedVitalSign = vitalSignRepository.save(currentVitalSign);
        return updatedVitalSign;
    }

    // Delete
    @Secured({"ROLE_USER"})
    @DeleteMapping("/vitalsigns/{id}")
    public ResponseEntity<?> deleteVitalSign(@PathVariable(value = "id") Long vitalSignid) {
        VitalSign vitalSign = vitalSignRepository.findById(vitalSignid)
                .orElseThrow(() -> new ResourceNotFoundException("VitalSign", "id", vitalSignid));

        vitalSignRepository.delete(vitalSign);

        return ResponseEntity.ok().build();
    }
}