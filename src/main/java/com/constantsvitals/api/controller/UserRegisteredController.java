package com.constantsvitals.api.controller;

import com.constantsvitals.api.exception.ResourceNotFoundException;
import com.constantsvitals.api.model.entity.UserRegistered;
import com.constantsvitals.api.repository.UserRegistredRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserRegisteredController {

    @Autowired
    UserRegistredRepository userRegistredRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    // Get All
    //@Secured({"ROLE_USER"})
    @GetMapping("/users")
    public List<UserRegistered> getAllUsers() {
        return userRegistredRepository.findAll();
    }

    //Get by username
    @Secured({"ROLE_USER"})
    @GetMapping("/users/byUsername/{username}")
    public UserRegistered getUserByUsername(@PathVariable(value = "username") String username) {
        return userRegistredRepository.findByUsername(username);
    }

    // Create
    //@Secured({"ROLE_USER"})
    @PostMapping("/users")
    public UserRegistered createUser(@Valid @RequestBody UserRegistered userRegistered) {
        userRegistered.setPassword(passwordEncoder.encode(userRegistered.getPassword()));
        return userRegistredRepository.save(userRegistered);
    }

    // Get a Single
    @Secured({"ROLE_USER"})
    @GetMapping("/users/{id}")
    public UserRegistered getUsersById(@PathVariable(value = "id") Long userRegistredId) {
        return userRegistredRepository.findById(userRegistredId)
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userRegistredId));
    }

    // Update
    @Secured({"ROLE_USER"})
    @PutMapping("/users/{id}")
    public UserRegistered updateUsers(@PathVariable(value = "id") Long userRegistredId,
                                      @Valid @RequestBody UserRegistered userRegistered) {

        UserRegistered currentUserRegistered = userRegistredRepository.findById(userRegistredId)
                .orElseThrow(() -> new ResourceNotFoundException("professional", "id", userRegistredId));

        currentUserRegistered.setUsername(currentUserRegistered.getUsername());
        currentUserRegistered.setNombre(currentUserRegistered.getNombre());
        currentUserRegistered.setApellido(currentUserRegistered.getApellido());
        currentUserRegistered.setEmail(currentUserRegistered.getEmail());
        currentUserRegistered.setEnabled(currentUserRegistered.getEnabled());
        currentUserRegistered.setPassword(currentUserRegistered.getPassword());
        currentUserRegistered.setRoles(currentUserRegistered.getRoles());

        UserRegistered updatedUserRegistered = userRegistredRepository.save(currentUserRegistered);
        return updatedUserRegistered;
    }

    // Delete
    @Secured({"ROLE_USER"})
    @DeleteMapping("/users/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable(value = "id") Long userRegistredId) {
        UserRegistered userRegistered = userRegistredRepository.findById(userRegistredId)
                .orElseThrow(() -> new ResourceNotFoundException("UserRegistered", "id", userRegistredId));

        userRegistredRepository.delete(userRegistered);

        return ResponseEntity.ok().build();
    }
}