package com.constantsvitals.api.controller;

import com.constantsvitals.api.exception.ResourceNotFoundException;
import com.constantsvitals.api.model.entity.Measurement;
import com.constantsvitals.api.repository.MeasurementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class MeasurementController {

    @Autowired
    MeasurementRepository measurementRepository;

    // Get All
    @Secured({"ROLE_USER"})
    @GetMapping("/measurements")
    public List<Measurement> getAllMeasurements() {
        return measurementRepository.findAll();
    }

    // Create
    @Secured({"ROLE_USER"})
    @PostMapping("/measurements")
    public Measurement createMeasurement(@Valid @RequestBody Measurement measurement) {
        measurement.setMsrIsRead(false);
        return measurementRepository.save(measurement);
    }

    // Get a Single
    @Secured({"ROLE_USER"})
    @GetMapping("/measurements/{id}")
    public Measurement getMeasurementById(@PathVariable(value = "id") Long measurementId) {
        return measurementRepository.findById(measurementId)
                .orElseThrow(() -> new ResourceNotFoundException("Measurement", "id", measurementId));
    }


    // Update
    @Secured({"ROLE_USER"})
    @PutMapping("/measurements/{id}")
    public Measurement updateMeasurement(@PathVariable(value = "id") Long measurementId,
                                     @Valid @RequestBody Measurement measurement) {

        Measurement currentMeasurement = measurementRepository.findById(measurementId)
                .orElseThrow(() -> new ResourceNotFoundException("Measurement", "id", measurementId));

        currentMeasurement.setCensus(measurement.getCensus());
        currentMeasurement.setUserRegistered(measurement.getUserRegistered());
        currentMeasurement.setMsrDateHourEnd(measurement.getMsrDateHourEnd());
        currentMeasurement.setMsrDateHourStart(measurement.getMsrDateHourStart());
        currentMeasurement.setMsrDateHourSetByProfessional(measurement.getMsrDateHourSetByProfessional());
        currentMeasurement.setMsrObservations(measurement.getMsrObservations());
        currentMeasurement.setMsrState(measurement.getMsrState());

        Measurement updatedMeasurment = measurementRepository.save(currentMeasurement);
        return updatedMeasurment;
    }

    // Delete
    @Secured({"ROLE_USER"})
    @DeleteMapping("/measurements/{id}")
    public ResponseEntity<?> deleteMeasurement(@PathVariable(value = "id") Long measurementId) {
        Measurement measurement = measurementRepository.findById(measurementId)
                .orElseThrow(() -> new ResourceNotFoundException("Measurement", "id", measurementId));

        measurementRepository.delete(measurement);

        return ResponseEntity.ok().build();
    }
}