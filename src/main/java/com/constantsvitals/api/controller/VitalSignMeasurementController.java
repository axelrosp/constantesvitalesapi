package com.constantsvitals.api.controller;

import com.constantsvitals.api.exception.ResourceNotFoundException;
import com.constantsvitals.api.model.entity.VitalSignMeasurement;
import com.constantsvitals.api.repository.VitalSignMeasurementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class VitalSignMeasurementController {

    @Autowired
    VitalSignMeasurementRepository vitalSignMeasurementRepository;

    // Get All
    @Secured({"ROLE_USER"})
    @GetMapping("/vitalsignmeasurements")
    public List<VitalSignMeasurement> getAllVitalSignMeasurements() {
        return vitalSignMeasurementRepository.findAll();
    }

    // Create
    @Secured({"ROLE_USER"})
    @PostMapping("/vitalsignmeasurements")
    public VitalSignMeasurement createVitalSignMeasurement(@Valid @RequestBody VitalSignMeasurement vitalSignMeasurement) {
        return vitalSignMeasurementRepository.save(vitalSignMeasurement);
    }

    // Get a Single
    @Secured({"ROLE_USER"})
    @GetMapping("/vitalsignmeasurements/{id}")
    public VitalSignMeasurement getVitalSignMeasurementById(@PathVariable(value = "id") Long vitalSignMeasurementId) {
        return vitalSignMeasurementRepository.findById(vitalSignMeasurementId)
                .orElseThrow(() -> new ResourceNotFoundException("VitalSignMeasurement", "id", vitalSignMeasurementId));
    }

    //Get all by censusId
    @Secured({"ROLE_USER"})
    @GetMapping("/vitalsignmeasurements/byCenId/{cenId}")
    public List<VitalSignMeasurement> getAllVitalSignMeasurementsByCenId(@PathVariable(value = "cenId") Long cenId) {
        return vitalSignMeasurementRepository.findByMeasurementCensusCenIdOrderByMeasurementMsrDateHourEndDesc(cenId);
    }

    // Update
    @Secured({"ROLE_USER"})
    @PutMapping("/vitalsignmeasurements/{id}")
    public VitalSignMeasurement updateVitalSignMeasurement(@PathVariable(value = "id") Long vitalSignMeasurementId,
                                                @Valid @RequestBody VitalSignMeasurement vitalSignMeasurement) {

        VitalSignMeasurement currentVitalSignMeasurement = vitalSignMeasurementRepository.findById(vitalSignMeasurementId)
                .orElseThrow(() -> new ResourceNotFoundException("VitalSignMeasurement", "id", vitalSignMeasurementId));

        currentVitalSignMeasurement.setMeasurement(vitalSignMeasurement.getMeasurement());
        currentVitalSignMeasurement.setVitalSign(vitalSignMeasurement.getVitalSign());
        currentVitalSignMeasurement.setVsmFilter(vitalSignMeasurement.getVsmFilter());
        currentVitalSignMeasurement.setVsmIsManual(vitalSignMeasurement.getVsmIsManual());
        currentVitalSignMeasurement.setVsmNemotechnical(vitalSignMeasurement.getVsmNemotechnical());
        currentVitalSignMeasurement.setVsmSystem(vitalSignMeasurement.getVsmSystem());
        currentVitalSignMeasurement.setVsmValue(vitalSignMeasurement.getVsmValue());


        VitalSignMeasurement updatedVitalSignMeasurment = vitalSignMeasurementRepository.save(currentVitalSignMeasurement);
        return updatedVitalSignMeasurment;
    }

    // Delete
    @Secured({"ROLE_USER"})
    @DeleteMapping("/vitalsignmeasurements/{id}")
    public ResponseEntity<?> deleteVitalSignMeasurement(@PathVariable(value = "id") Long vitalSignMeasurementId) {
        VitalSignMeasurement vitalSignMeasurement = vitalSignMeasurementRepository.findById(vitalSignMeasurementId)
                .orElseThrow(() -> new ResourceNotFoundException("VitalSignMeasurement", "id", vitalSignMeasurementId));

        vitalSignMeasurementRepository.delete(vitalSignMeasurement);

        return ResponseEntity.ok().build();
    }
}