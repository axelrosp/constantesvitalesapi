package com.constantsvitals.api.controller;

import com.constantsvitals.api.exception.ResourceNotFoundException;
import com.constantsvitals.api.model.entity.VitalSignType;
import com.constantsvitals.api.repository.VitalSignTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class VitalSignTypeController {

    @Autowired
    VitalSignTypeRepository vitalSignTypeRepository;

    // Get All
    @Secured({"ROLE_USER"})
    @GetMapping("/vitalsigntypes")
    public List<VitalSignType> getAllVitalSignTypes() {
        return vitalSignTypeRepository.findAll();
    }

    // Create
    @Secured({"ROLE_USER"})
    @PostMapping("/vitalsigntypes")
    public VitalSignType createVitalSignType(@Valid @RequestBody VitalSignType vitalSignType) {
        return vitalSignTypeRepository.save(vitalSignType);
    }

    // Get a Single
    @Secured({"ROLE_USER"})
    @GetMapping("/vitalsigntypes/{id}")
    public VitalSignType getVitalSignTypeById(@PathVariable(value = "id") Long vstId) {
        return vitalSignTypeRepository.findById(vstId)
                .orElseThrow(() -> new ResourceNotFoundException("VitalSign", "id", vstId));
    }

    // Update
    @Secured({"ROLE_USER"})
    @PutMapping("/vitalsigntypes/{id}")
    public VitalSignType updateVitalSign(@PathVariable(value = "id") Long vstId,
                                     @Valid @RequestBody VitalSignType vitalSignType) {

        VitalSignType currentVitalSignType = vitalSignTypeRepository.findById(vstId)
                .orElseThrow(() -> new ResourceNotFoundException("VitalSign", "id", vstId));

        currentVitalSignType.setVstName(vitalSignType.getVstName());

        VitalSignType updatedVitalSign = vitalSignTypeRepository.save(currentVitalSignType);
        return updatedVitalSign;
    }

    // Delete
    @Secured({"ROLE_USER"})
    @DeleteMapping("/vitalsigntypes/{id}")
    public ResponseEntity<?> deleteVitalSign(@PathVariable(value = "id") Long vstId) {
        VitalSignType vitalSignType = vitalSignTypeRepository.findById(vstId)
                .orElseThrow(() -> new ResourceNotFoundException("VitalSign", "id", vstId));

        vitalSignTypeRepository.delete(vitalSignType);

        return ResponseEntity.ok().build();
    }
}