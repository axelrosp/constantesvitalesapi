package com.constantsvitals.api.controller;

import com.constantsvitals.api.exception.ResourceNotFoundException;
import com.constantsvitals.api.model.entity.Census;
import com.constantsvitals.api.model.DTO.CensusDTO;
import com.constantsvitals.api.model.entity.Measurement;
import com.constantsvitals.api.repository.CensusRepository;
import com.constantsvitals.api.repository.MeasurementRepository;
import com.constantsvitals.api.utils.TimeCustomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CensusController {

    @Autowired
    CensusRepository censusRepository;

    @Autowired
    MeasurementRepository measurementRepository;

    // Get All
    @Secured({"ROLE_USER"})
    @GetMapping("/census")
    public List<Census> getAllCensus() {
       return censusRepository.findAll();
    }

    //Get census by floor
    @Secured({"ROLE_USER"})
    @GetMapping("/census/byFloor/{floor}")
    public List<CensusDTO> getCensusByFloor(@PathVariable(value = "floor") int censusFloor){
        List<CensusDTO> censusReturn = new ArrayList<>();
        List<Census> cenByFloor = censusRepository.findBycenNumFloorOrderByCenNumRoomAsc(censusFloor);
        for(Census cen : cenByFloor ){
            Measurement m = measurementRepository.findFirstByCensusOrderByMsrDateHourEndDesc(cen);
            CensusDTO censusDTO = new CensusDTO(cen, null);
            if(m!=null){
                if(TimeCustomUtils.diffBetweenDatesMinutes(
                        m.getMsrDateHourEnd(), new Timestamp(
                                System.currentTimeMillis())) < TimeCustomUtils.MAX_MINUTES_VISITED_TO_GET_SHOWN) {
                    censusDTO.setMsrDateHourEnd(m.getMsrDateHourEnd());
                }
            }

            censusReturn.add(censusDTO);
        }

        return censusReturn;

    }

    // Create
    @Secured({"ROLE_USER"})
    @PostMapping("/census")
    public Census createCensus(@Valid @RequestBody Census census) {
        return censusRepository.save(census);
    }

    // Create List
    @Secured({"ROLE_USER"})
    @PostMapping("/censusList")
    public List<Census> createCensusList(@Valid @RequestBody List<Census> census) {
        return censusRepository.saveAll(census);
    }

    // Get a Single
    @Secured({"ROLE_USER"})
    @GetMapping("/census/{id}")
    public Census getCensusById(@PathVariable(value = "id") Long censusId) {
        return censusRepository.findById(censusId)
                .orElseThrow(() -> new ResourceNotFoundException("Census", "id", censusId));
    }

    // Update
    @Secured({"ROLE_USER"})
    @PutMapping("/census/{id}")
    public Census updateCensus(@PathVariable(value = "id") Long censusId,
                                           @Valid @RequestBody Census census) {

        Census currentCensus = censusRepository.findById(censusId)
                .orElseThrow(() -> new ResourceNotFoundException("VitalSign", "id", censusId));

        currentCensus.setCenNumFloor(census.getCenNumFloor());
        currentCensus.setCenNumHistory(census.getCenNumHistory());
        currentCensus.setCenNumRoom(census.getCenNumRoom());
        currentCensus.setCenNamePatient(census.getCenNamePatient());
        currentCensus.setCenSpeciality(census.getCenSpeciality());
        currentCensus.setCenService(census.getCenService());

        Census updatedCensus = censusRepository.save(currentCensus);
        return updatedCensus;
    }

    // Delete
    @Secured({"ROLE_USER"})
    @DeleteMapping("/census/{id}")
    public ResponseEntity<?> deleteCensus(@PathVariable(value = "id") Long censusId) {
        Census census = censusRepository.findById(censusId)
                .orElseThrow(() -> new ResourceNotFoundException("Census", "id", censusId));

        censusRepository.delete(census);

        return ResponseEntity.ok().build();
    }
}