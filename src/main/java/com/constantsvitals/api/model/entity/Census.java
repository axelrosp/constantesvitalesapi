package com.constantsvitals.api.model.entity;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Entity
@Table(name = "census")
@EntityListeners(AuditingEntityListener.class)
public class Census implements Serializable {

    @Id
    @GeneratedValue(strategy     = GenerationType.IDENTITY)
    @Column(name = "cen_id")
    private Long cenId;

    @NotBlank
    @Column(unique = true, nullable = false, name="cen_num_history")
    private String cenNumHistory;

    @NotBlank
    @Column(nullable = false, name="cen_name_patient")
    private String cenNamePatient;

    @NotBlank
    @Column(nullable = false, name="cen_num_room")
    private String cenNumRoom;

    @Column(nullable = false, name="cen_num_floor")
    private int cenNumFloor;

    @NotBlank
    @Column(nullable = false, name="cen_speciality")
    private String cenSpeciality;

    @NotBlank
    @Column(nullable = false, name="cen_service")
    private String cenService;

    public Long getCenId() {
        return cenId;
    }

    public String getCenNumHistory() {
        return cenNumHistory;
    }

    public void setCenNumHistory(String cenNumHistory) {
        this.cenNumHistory = cenNumHistory;
    }

    public String getCenNamePatient() {
        return cenNamePatient;
    }

    public void setCenNamePatient(String cenNamePatient) {
        this.cenNamePatient = cenNamePatient;
    }

    public String getCenNumRoom() {
        return cenNumRoom;
    }

    public void setCenNumRoom(String cenNumRoom) {
        this.cenNumRoom = cenNumRoom;
    }

    public int getCenNumFloor() {
        return cenNumFloor;
    }

    public void setCenNumFloor(int cenNumFloor) {
        this.cenNumFloor = cenNumFloor;
    }

    public String getCenSpeciality() {
        return cenSpeciality;
    }

    public void setCenSpeciality(String cenSpeciality) {
        this.cenSpeciality = cenSpeciality;
    }

    public String getCenService() {
        return cenService;
    }

    public void setCenService(String cenService) {
        this.cenService = cenService;
    }
}