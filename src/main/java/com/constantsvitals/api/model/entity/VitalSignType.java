package com.constantsvitals.api.model.entity;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Entity
@Table(name = "vital_sign_type")
@EntityListeners(AuditingEntityListener.class)
public class VitalSignType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "vst_id")
    private Long vstId;

    @NotBlank
    @Column(nullable = false, name = "vst_name")
    private String vstName;

    public Long getVstId() {
        return vstId;
    }

    public String getVstName() {
        return vstName;
    }

    public void setVstName(String vstName) {
        this.vstName = vstName;
    }
}