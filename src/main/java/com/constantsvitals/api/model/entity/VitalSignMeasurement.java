package com.constantsvitals.api.model.entity;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "vital_sign_measurement")
@EntityListeners(AuditingEntityListener.class)
public class VitalSignMeasurement implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "vsm_id")
    private Long vsmId;

    @ManyToOne
    private Measurement measurement;

    @ManyToOne
    private VitalSign vitalSign;

    @Column(nullable = false, name="vsm_value")
    private String vsmValue;

    @Column(nullable = false, name="vsm_nemotechnical")
    private String vsmNemotechnical;

    @Column(name="vsm_is_manual")
    private Boolean vsmIsManual;

    @Column(name="vsm_System")
    private String vsmSystem;

    @Column(name="vsm_Filter")
    private Double vsmFilter;

    public Long getVsmId() {
        return vsmId;
    }

    public Measurement getMeasurement() {
        return measurement;
    }

    public void setMeasurement(Measurement measurement) {
        this.measurement = measurement;
    }

    public VitalSign getVitalSign() {
        return vitalSign;
    }

    public void setVitalSign(VitalSign vitalSign) {
        this.vitalSign = vitalSign;
    }

    public String getVsmValue() {
        return vsmValue;
    }

    public void setVsmValue(String vsmValue) {
        this.vsmValue = vsmValue;
    }

    public String getVsmNemotechnical() {
        return vsmNemotechnical;
    }

    public void setVsmNemotechnical(String vsmNemotechnical) {
        this.vsmNemotechnical = vsmNemotechnical;
    }

    public Boolean getVsmIsManual() {
        return vsmIsManual;
    }

    public void setVsmIsManual(Boolean vsmIsManual) {
        this.vsmIsManual = vsmIsManual;
    }

    public String getVsmSystem() {
        return vsmSystem;
    }

    public void setVsmSystem(String vsmSystem) {
        this.vsmSystem = vsmSystem;
    }

    public Double getVsmFilter() {
        return vsmFilter;
    }

    public void setVsmFilter(Double vsmFilter) {
        this.vsmFilter = vsmFilter;
    }
}