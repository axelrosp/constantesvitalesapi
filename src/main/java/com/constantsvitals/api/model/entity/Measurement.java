package com.constantsvitals.api.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "measurement")
@EntityListeners(AuditingEntityListener.class)
public class Measurement implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "msrId")
    private Long msrId;

    @ManyToOne
    private UserRegistered userRegistered;

    @ManyToOne
    private Census census;

    @Temporal(value = TemporalType.TIMESTAMP)
    @JsonFormat(timezone = "Europe/Madrid")
    @Column(nullable = false, name="msr_date_hour_start")
    private Date msrDateHourStart;

    @Temporal(value = TemporalType.TIMESTAMP)
    @JsonFormat(timezone = "Europe/Madrid")
    @Column(nullable = false, name="msr_date_hour_end")
    private Date msrDateHourEnd;

    @Temporal(value = TemporalType.TIMESTAMP)
    @JsonFormat(timezone = "Europe/Madrid")
    @Column(nullable = false, name="msr_date_hour_set_by_professional")
    private Date msrDateHourSetByProfessional;

    @Column(name = "msr_state")
    private String msrState;

    @Column(name = "msr_observations")
    private String msrObservations;

    @Column(name = "msr_is_read", columnDefinition="tinyint(1) default 0")
    private Boolean msrIsRead;

    public Long getMsrId() {
        return msrId;
    }

    public UserRegistered getUserRegistered() {
        return userRegistered;
    }

    public void setUserRegistered(UserRegistered userRegistered) {
        this.userRegistered = userRegistered;
    }


    public Census getCensus() {
        return census;
    }

    public void setCensus(Census census) {
        this.census = census;
    }


    public Date getMsrDateHourStart() {
        return msrDateHourStart;
    }

    public void setMsrDateHourStart(Date msrDateHourStart) {
        this.msrDateHourStart = msrDateHourStart;
    }

    public Date getMsrDateHourEnd() {
        return msrDateHourEnd;
    }

    public void setMsrDateHourEnd(Date msrDateHourEnd) {
        this.msrDateHourEnd = msrDateHourEnd;
    }

    public Date getMsrDateHourSetByProfessional() {
        return msrDateHourSetByProfessional;
    }

    public void setMsrDateHourSetByProfessional(Date msrDateHourSetByProfessional) {
        this.msrDateHourSetByProfessional = msrDateHourSetByProfessional;
    }

    public String getMsrState() {
        return msrState;
    }

    public void setMsrState(String msrState) {
        this.msrState = msrState;
    }

    public String getMsrObservations() {
        return msrObservations;
    }

    public void setMsrObservations(String msrObservations) {
        this.msrObservations = msrObservations;
    }

    public Boolean getMsrIsRead() {
        return msrIsRead;
    }

    public void setMsrIsRead(Boolean msrIsRead) {
        this.msrIsRead = msrIsRead;
    }
}


