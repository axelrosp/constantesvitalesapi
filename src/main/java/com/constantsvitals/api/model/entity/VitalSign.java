package com.constantsvitals.api.model.entity;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Entity
@Table(name = "vital_sign")
@EntityListeners(AuditingEntityListener.class)
public class VitalSign implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "vs_id")
    private Long vsId;

    @ManyToOne
    private VitalSignType vitalSignType;

    @NotBlank
    @Column(nullable = false, name="vs_name")
    private String vsName;

    @NotBlank
    @Column(nullable = false, name="vs_nemotechnical")
    private String vsNemotechnical;

    @Column(name="vs_limit_max_value")
    private int vsLimitMaxValue;

    @Column(name="vs_limit_min_value")
    private int vsLimitMinValue;


    public Long getVsId() {
        return vsId;
    }

    public VitalSignType getVitalSignType() {
        return vitalSignType;
    }

    public void setVitalSignType(VitalSignType vitalSignType) {
        this.vitalSignType = vitalSignType;
    }

    public String getVsName() {
        return vsName;
    }

    public void setVsName(String vsName) {
        this.vsName = vsName;
    }

    public String getVsNemotechnical() {
        return vsNemotechnical;
    }

    public void setVsNemotechnical(String vsNemotechnical) {
        this.vsNemotechnical = vsNemotechnical;
    }

    public int getVsLimitMaxValue() {
        return vsLimitMaxValue;
    }

    public void setVsLimitMaxValue(int vsLimitMaxValue) {
        this.vsLimitMaxValue = vsLimitMaxValue;
    }

    public int getVsLimitMinValue() {
        return vsLimitMinValue;
    }

    public void setVsLimitMinValue(int vsLimitMinValue) {
        this.vsLimitMinValue = vsLimitMinValue;
    }
}