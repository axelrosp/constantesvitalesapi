package com.constantsvitals.api.model.repository;

import com.constantsvitals.api.model.entity.UserRegistered;
import org.springframework.data.repository.CrudRepository;

public interface IUsuarioDao extends CrudRepository<UserRegistered, Long> {
	UserRegistered findByUsername(String username);
}
