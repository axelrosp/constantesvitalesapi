package com.constantsvitals.api.model.DTO;
import com.constantsvitals.api.model.entity.Census;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

public class CensusDTO {

    private Census census;

    @Temporal(value = TemporalType.TIMESTAMP)
    @JsonFormat(timezone = "Europe/Madrid")
    private Date msrDateHourEnd;

    public CensusDTO(Census census, Date msrDateHourEnd){
        this.census = census;
        this.msrDateHourEnd = msrDateHourEnd;
    }

    public Census getCensus() {
        return census;
    }

    public void setCensus(Census census) {
        this.census = census;
    }

    public Date getMsrDateHourEnd() {
        return msrDateHourEnd;
    }

    public void setMsrDateHourEnd(Date msrDateHourEnd) {
        this.msrDateHourEnd = msrDateHourEnd;
    }
}
