package com.constantsvitals.api.auth;


import com.constantsvitals.api.model.entity.UserRegistered;
import com.constantsvitals.api.auth.services.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class InfoAdicionalToken implements TokenEnhancer {
	
	@Autowired
	private IUsuarioService usuarioService;

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		
		UserRegistered userRegistered = usuarioService.findByUsername(authentication.getName());

		System.out.println("NAME" + authentication.getName());

		Map<String, Object> info = new HashMap<>();
		info.put("info_adicional", "Hola que tal!: ".concat(authentication.getName()));
		
		info.put("nombre", userRegistered.getNombre());
		info.put("apellido", userRegistered.getApellido());
		info.put("username", userRegistered.getUsername());
		info.put("email", userRegistered.getEmail());
		
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);



		
		return accessToken;
	}

}
