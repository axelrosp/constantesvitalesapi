package com.constantsvitals.api.auth.services;


import com.constantsvitals.api.model.entity.UserRegistered;

public interface IUsuarioService {
	UserRegistered findByUsername(String username);
}
