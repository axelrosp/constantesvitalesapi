package com.constantsvitals.api.utils;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TimeCustomUtils {
    public static final int MAX_MINUTES_VISITED_TO_GET_SHOWN = 30;
    public static long diffBetweenDatesMinutes(Date old, Date now){
        long duration  = now.getTime() - old.getTime();
        return TimeUnit.MILLISECONDS.toMinutes(duration);
    }
}
